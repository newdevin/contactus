using System;
using Xunit;

namespace ContactUs.Domain.Test
{
    public class ContactTest
    {
        [Theory]
        [InlineData(null, "someone@domain.com", "Hello message")]
        [InlineData("", "someone@domain.com", "Hello message")]
        [InlineData("someone", null, "Hello message")]
        [InlineData("someone", "", "Hello message")]
        [InlineData("someone", "someone.com", "Hello message")]
        [InlineData("someone", "someone@domain.com", "")]
        public void GivenInvalidArgumentsToCreateMethod_ExeptionIsThrown(string name, string email, string message)
        {
            Assert.Throws<ArgumentException>(() => Contact.Create(name, email, message));

        }

        [Fact]
        public void GivenValidArgumentsCreateMethod_ContactObjectIsCreated()
        {
            Contact contact = Contact.Create("someone", "someone@domain.com", "Hello message");
            Assert.NotNull(contact);
        }
    }
}
