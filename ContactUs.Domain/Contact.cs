﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace ContactUs.Domain
{
    public class Contact
    {
        public static Contact Create(string name, string email, string message)
        {
            if (!IsValidName(name))
                throw new ArgumentException($"{nameof(name)} is not valid");
            if(!IsValidEmail(email))
                throw new ArgumentException($"{nameof(email)} is not valid" );
            if (!IsValidMessage(message))
                throw new ArgumentException($"{nameof(message)} is not valid");

            return new Contact(name, email, message);

        }

        private static bool IsValidMessage(string message)
        {
            return !string.IsNullOrEmpty(message);
        }

        private static bool IsValidName(string name)
        {
            return !string.IsNullOrEmpty(name);
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch 
            {
                return false;
            }
        }
        private Contact(string name, string email, string message)
        {
            Name = name;
            Email = email;
            Message = message;
        }

        public string Name { get; }
        public string Email { get; }
        public string Message { get; }
    }
}
