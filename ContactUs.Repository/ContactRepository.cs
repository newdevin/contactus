﻿using ContactUs.Domain;
using ContactUs.Repository.Entities;
using ContectUs.Service.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactUs.Repository
{
    public class ContactRepository : IContactRepository
    {
        private readonly ContactUsDbContext contactUsDbContext;

        public ContactRepository(ContactUsDbContext contactUsDbContext)
        {
            this.contactUsDbContext = contactUsDbContext;
        }
        public async Task<Contact> AddMessage(Contact contact)
        {
            var entity = new ContactEntity
            {
                Email = contact.Email,
                Message = contact.Message,
                Name = contact.Name
            };

            contactUsDbContext.Contacts.Add(entity);
            await contactUsDbContext.SaveChangesAsync();
            return contact;
        }

        public async Task<IEnumerable<Contact>> GetAllMessages()
        {
            var entities = await contactUsDbContext.Contacts.ToListAsync();
            return entities.Select(e => Contact.Create(e.Name, e.Email, e.Message));
        }
    }
}
