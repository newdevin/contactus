﻿using ContactUs.Repository.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContactUs.Repository
{
    public class ContactUsDbContext : DbContext
    {
        public DbSet<ContactEntity> Contacts { get; set; }
        public ContactUsDbContext(DbContextOptions options) : base(options)
        {

        }

    }
}
