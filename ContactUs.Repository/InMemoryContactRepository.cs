﻿using ContactUs.Domain;
using ContectUs.Service.Repository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactUs.Repository
{
    public class InMemoryContactRepository : IContactRepository
    {
        private static List<Contact> contacts = new List<Contact>();

        public Task<Contact> AddMessage(Contact contact)
        {
            contacts.Add(contact);
            return Task.FromResult(contact);
        }

        public Task<IEnumerable<Contact>> GetAllMessages()
        {
            return Task.FromResult<IEnumerable<Contact>>(contacts);
        }
    }
}
