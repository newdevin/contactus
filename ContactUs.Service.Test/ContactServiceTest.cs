using ContactUs.Repository;
using System;
using Xunit;
using Microsoft.EntityFrameworkCore;
using ContectUs.Service;
using ContactUs.Domain;
using System.Linq;

namespace ContactUs.Service.Test
{
    public class ContactServiceTest
    {
        ContactUsDbContext context;
        ContactService service;
        
        
        public ContactServiceTest()
        {
            var options = new DbContextOptionsBuilder<ContactUsDbContext>()
                      .UseInMemoryDatabase(Guid.NewGuid().ToString())
                      .Options;
            context = new ContactUsDbContext(options);

            var repo = new ContactRepository(context);
            service = new ContactService(repo);
            
        }
        [Fact]
        public void ContactUsMessageIsAddedSuccessfullyToDatabase()
        {
            var name = "Adam Smith";
            var email = "adam@microsoft.com";
            var message = "Hi, I would like to say hello to you all";
            var contact = Contact.Create(name, email, message);
            _ = service.AddMessage(contact).GetAwaiter().GetResult();

            Assert.Equal(1, context.Contacts.Count());
            var entity = context.Contacts.First();
            Assert.NotNull(entity);
            Assert.Equal(name, entity.Name);
            Assert.Equal(email, entity.Email);
            Assert.Equal(message, entity.Message);


        }
    }
}
