import { Component, OnInit } from '@angular/core';
import { ContactUsService } from '../services/contact-us.service';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  constructor(private contactUsService:ContactUsService) { }


  name:string
  email:string
  message:string
  submitted:boolean

  ngOnInit() {
  }

  async sendMessage(){
    await this.contactUsService.addMessage(this.name,this.email, this.message).toPromise()
    this.submitted = true;
  }

}
