import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ContactUsService {

  constructor(private http:HttpClient) { }

  addMessage(name:string, email:string, message:string){
    var apiUrl = environment.apiUrl;
    return this.http.post(`${apiUrl}/contactus`, {name:name, email:email, message:message});
  }
}
