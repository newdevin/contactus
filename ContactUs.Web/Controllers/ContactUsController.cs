﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactUs.Domain;
using ContactUs.Web.Dtos;
using ContectUs.Service;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ContactUs.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ContactUsController : ControllerBase
    {
        private readonly IContactService contactService;

        public ContactUsController(IContactService contactService)
        {
            this.contactService = contactService;
        }

        [HttpPost]
        public async Task<IActionResult> AddMessage([FromBody] ContactDto contactDto)
        {
            try
            {
                var contact = Contact.Create(contactDto.Name, contactDto.Email, contactDto.Message);
                var createdContact = await contactService.AddMessage(contact);
                return Ok(createdContact);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> GetAllMessages()
        {
            var contacts = await contactService.GetAllMessages();
            var contactDtos = contacts.Select(c => Contact.Create(c.Name, c.Email, c.Message));
            return Ok(contactDtos);
        }
    }
}