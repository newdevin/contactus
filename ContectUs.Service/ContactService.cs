﻿using ContactUs.Domain;
using ContectUs.Service.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContectUs.Service
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository contactRepository;

        public ContactService(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }
        public Task<Contact> AddMessage(Contact contact)
        {
            return contactRepository.AddMessage(contact);
        }

        public Task<IEnumerable<Contact>> GetAllMessages()
        {
            return contactRepository.GetAllMessages();
        }
    }
}
