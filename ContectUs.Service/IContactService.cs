﻿using ContactUs.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContectUs.Service
{
    public interface IContactService
    {
        Task<Contact> AddMessage(Contact contact);

        Task<IEnumerable<Contact>> GetAllMessages();
    }
}
