﻿using ContactUs.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ContectUs.Service.Repository
{
    public interface IContactRepository
    {
        Task<Contact> AddMessage(Contact contact);
        Task<IEnumerable<Contact>> GetAllMessages();
    }
}
